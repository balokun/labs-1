﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;
namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMałpa);
        
        public static Type ISub1 = typeof(IGoryl);
        public static Type Impl1 = typeof(Impl1);
        
        public static Type ISub2 = typeof(IPawian);
        public static Type Impl2 = typeof(Impl2);


        public static string baseMethod = "PrzedstawSie";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Władza";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Relax";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "metoda1";
        public static string collectionConsumerMethod = "metoda2";

        #endregion

        #region P3

        public static Type IOther = typeof(IOlbrzym);
        public static Type Impl3 = typeof(Impl3);

        public static string otherCommonMethod = "PrzedstawSie";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
