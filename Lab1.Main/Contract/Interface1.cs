﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    interface IMałpa
    {
        string PrzedstawSie();
    }
    interface IGoryl : IMałpa
    {
        string Władza();
    }
    interface IPawian : IMałpa
    {
        string Relax();
    }
    interface IOlbrzym
    {
        string PrzedstawSie();
        string Inny();
    }
}
